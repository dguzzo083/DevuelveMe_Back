desc 'warning email'
task warning_email: :environment do
  loans = Loan.all

  loans.each do |loan|
    friend = Friend.find(loan.friend_id)
    book = Book.find(loan.book_id)
    if loan.deadline == (DateTime.now + 5) && loan.status == "Por devolver" && loan.prorroga == "No solicitada"
    UserMailer.warning_email(friend, loan, book).deliver
    @sms = Textveloper::Sdk.new("0bc6e4c721c02316fb5c9e44e36cfc89","ec0994b8ab66f57c2aab6d3c213cd5c8")
    @sms.send_sms(friend.phone,"DevuelveMe Aplication le informa que usted posee un prestamo a punto de vencer, por favor consulte su correo electronico")
    elsif loan.prorroga == "Solicitada" && loan.deadline == (DateTime.now + 5)
    UserMailer.no_prorroga(friend, loan, book).deliver
    @sms = Textveloper::Sdk.new("0bc6e4c721c02316fb5c9e44e36cfc89","ec0994b8ab66f57c2aab6d3c213cd5c8")
    @sms.send_sms(friend.phone,"DevuelveMe Aplication le informa que usted posee un prestamo a punto de vencer, por favor consulte su correo electronico")
    end
  end
end