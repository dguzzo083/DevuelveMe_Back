# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150928233610) do

  create_table "books", force: :cascade do |t|
    t.string   "title",                             null: false
    t.string   "author",                            null: false
    t.string   "isbn",                              null: false
    t.string   "status",     default: "Disponible"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "friends", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "last_name"
    t.string   "email",      null: false
    t.string   "phone",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "loans", force: :cascade do |t|
    t.integer  "friend_id"
    t.integer  "book_id"
    t.date     "deadline",                             null: false
    t.string   "prorroga",   default: "No solicitada"
    t.string   "status",     default: "Por devolver"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "loans", ["book_id"], name: "index_loans_on_book_id"
  add_index "loans", ["friend_id"], name: "index_loans_on_friend_id"

end
