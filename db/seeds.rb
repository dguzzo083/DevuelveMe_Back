# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Friend.create(name: "Daniel", last_name: "Guzzo", phone: "04143266250", email: "danieljesusguzzoperez@gmail.com")
Friend.create(name: "Nelson", last_name: "Marcano", phone: "04242716069", email: "nerumarcano@gmail.com")
Friend.create(name: "Johanna", last_name: "Salazar", phone: "04123956948", email: "johanna.salazar.b@gmail.com")
Friend.create(name: "Scarleth", last_name: "Bernal", phone: "04143333333", email: "scarleth1306@gmail.com")


Book.create(title: "Libro 1", author: "Autor 1", isbn: "HACK-3984950", status: "Prestado")
Book.create(title: "Libro 2", author: "Autor 2", isbn: "HACK-9247350", status: "Prestado")
Book.create(title: "Libro 3", author: "Autor 3", isbn: "HACK-9284736", status: "Prestado")
Book.create(title: "Libro 4", author: "Autor 4", isbn: "HACK-0082743", status: "Prestado")

Loan.create(friend_id: 1, book_id: 1)
Loan.create(friend_id: 2, book_id: 2)
Loan.create(friend_id: 3, book_id: 3)
Loan.create(friend_id: 4, book_id: 4)