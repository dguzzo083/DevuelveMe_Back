class CreateFriends < ActiveRecord::Migration
  def change
    create_table :friends do |t|
      t.string :name, null: false
      t.string :last_name
      t.string :email, null: false
      t.string :phone, null: false

      t.timestamps null: false
    end
  end
end
