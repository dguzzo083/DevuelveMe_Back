class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.references :friend, index: true, foreign_key: true
      t.references :book, index: true, foreign_key: true
      t.date :deadline, null: false
      t.string :prorroga, default: "No solicitada"
      t.string :status, default: "Por devolver"

      t.timestamps null: false
    end
  end
end
