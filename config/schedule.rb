set :enviroment, "development"
set :output, {:error => "log/cron_error_log.log", :standard => "log/cron_log.log"}

every :day, :at => '12:00 pm' do 
  rake "warning_email:warning_email"
end
