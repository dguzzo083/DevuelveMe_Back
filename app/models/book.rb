class Book < ActiveRecord::Base
  validates :title, :author, :isbn, presence: true
  validates :isbn, uniqueness: true
  has_many :loans

  def change_status
    if self.status == "Disponible"
      self.status = "Prestado"    
    else
      self.status = "Disponible"
    end
  end

end
