class Loan < ActiveRecord::Base
  belongs_to :friend
  belongs_to :book
  before_save :create_deadline

  def create_deadline
    date = DateTime.now
    date = (date+30)
    self.deadline = date
  end

  def set_new_deadline
    date = DateTime.now
    date = (date+10)
    self.deadline = date
    self.prorroga = "Solicitada"
  end

  def status_to_returned
    self.status = "Devuelto"
  end
end
