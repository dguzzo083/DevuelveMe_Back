class Friend < ActiveRecord::Base
  validates :name, :phone, :email, presence: true
  has_many :loans
end
