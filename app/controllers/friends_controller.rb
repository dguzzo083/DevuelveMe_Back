class FriendsController < ApplicationController
  def index
    friends = Friend.all
    render json: friends
  end

  def create
    friend = Friend.new(permit_params)
    if friend.save
      render json: friend
    else
      render json: {errors: friend.errors.full_messages, message: "El amigo no se ha registrado"}, status: :bad_request
    end
  end

  def update
    begin
      friend = Friend.find(params[:id])
      
      if friend.update(permit_params)
        render json: friend
      else
        render json: {errors: friend.errors.full_messages, message: "Datos no actualizados"}, status: :bad_request
      end
    
    rescue ActiveRecord::RecordNotFound
      render json: {message: "El amigo no existe"}, status: :bad_request
    end
  end

  def permit_params
    if params[:friend].present? 
      params.require(:friend).permit(:name, :last_name, :phone, :email)
    end
  end
end
