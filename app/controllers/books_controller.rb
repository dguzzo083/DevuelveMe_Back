class BooksController < ApplicationController
  def index
    books = Book.all
    render json: books
  end

  def create
    book = Book.new(permit_params)
    if book.save
      render json: book
    else
      render json: {errors: book.errors.full_messages, message: "El libro no se ha registrado"}, status: :bad_request
    end
  end

  def update
    begin
      book = Book.find(params[:id])
      
      if book.update(permit_params)
        render json: book
      else
        render json: {errors: book.errors.full_messages, message: "Datos no actualizados"}, status: :bad_request
      end
    
    rescue ActiveRecord::RecordNotFound
      render json: {message: "El libro no existe"}, status: :bad_request
    end
  end

  def permit_params
    if params[:book].present? 
      params.require(:book).permit(:title, :author, :isbn)
    end
  end
end
