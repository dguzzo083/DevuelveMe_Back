class LoansController < ApplicationController
  def index
    @loans = Loan.all
    generate_index_hash
    render json: @loan_array
  end

  def create
    @loan = Loan.new(permit_params)
    if @loan.save
      change_book_status
      render json: @loan
    else
      render json: {errors: @loan.errors.full_messages, message: "El prestamo no fue realizado"}, status: :bad_request
    end
  end

  def update
    begin
      @loan = Loan.find(params[:id])
      
      if @loan
        @loan.status_to_returned
        change_book_status
        @loan.save
        render json: @loan
      else
        render json: {errors: @loan.errors.full_messages, message: "Datos no actualizados"}, status: :bad_request
      end
    
    rescue ActiveRecord::RecordNotFound
      render json: {message: "El prestamo no existe"}, status: :bad_request
    end
  end

  def permit_params
    if params[:loan].present? 
      params.require(:loan).permit(:friend_id, :book_id)
    end
  end

  def generate_index_hash
    @loan_array = []
    
    @loans.each do |loan|
      friend = Friend.find(loan.friend_id)
      book = Book.find(loan.book_id)
      hash = {id: loan.id, name: friend.name, last_name: friend.last_name, friend_id: friend.id, book: book.title, created_at: loan.created_at, deadline: loan.deadline, status: loan.status, prorroga: loan.prorroga}
      @loan_array << hash
   end
    @loan_array 
  end

  def change_book_status
    book = Book.find(@loan.book_id)
    book.change_status
    book.save
  end

  def prorroga
    loan = Loan.find(params[:loan_id])
    loan.set_new_deadline
    render json: loan
  end

end
