class UserMailer < ApplicationMailer
  default from: 'danielitox222@gmail.com'
 
  def warning_email(friend, loan, book)
    @friend = friend
    @loan = loan
    @book = book
    mail(to: @friend.email, subject: 'Correo de Advertencia DevuelveMe Aplication')
  end

  def no_prorroga(friend, loan, book)
    @friend = friend
    @loan = loan
    @book = book
    mail(to: @friend.email, subject: 'Su prorroga esta por vencer')
  end

end